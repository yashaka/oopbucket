package oopbucket.core;

/**
 * @author ykramarenko
 *         Date: 2/11/14
 *         Time: 7:31 PM
 */
public class Element {
    private String element;

    public Element(String element){
        this.element = element;
    }

    public Element should(Condition condition){
        condition.apply(this);
        return this;
    }

    @Override
    public String toString(){
        return element;
    }
}
