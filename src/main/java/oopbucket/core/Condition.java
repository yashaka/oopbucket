package oopbucket.core;

/**
 * @author ykramarenko
 *         Date: 2/11/14
 *         Time: 7:45 PM
 */
public class Condition {
    private String condition;

    public Condition(String condition){
        this.condition = condition;
    }

    public boolean apply(Element element){
        return true;
    }

    @Override
    public String toString(){
        return condition;
    }
}
