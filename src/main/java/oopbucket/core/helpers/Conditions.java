package oopbucket.core.helpers;

import oopbucket.core.Condition;
import oopbucket.core.Element;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 2:45 PM
 */
public class Conditions {

    public static void shouldBe(Condition condition){
        Common.debugLog("asserting " + condition);
    }
    public static void shouldBe(Element element, Condition condition){
        Common.debugLog("asserting " + element + " via " + condition);
    }

    public static boolean is(Condition condition){
        Common.debugLog("checking " + condition);
        return true;
    }
}
