package oopbucket.core.helpers;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 2:40 PM
 */
public class Common {

    public static void debugLog(String message){
        System.out.println(String.format("DEBUG INFO: %s: %s: ",
                System.currentTimeMillis(),
                message));
    }
}
