package oopbucket.oop;

import oopbucket.core.Condition;
import oopbucket.core.Element;

import static oopbucket.core.helpers.Common.debugLog;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 2:39 PM
 */
public class Page extends Component{
    public static final Condition saved = new Condition("saved");

    public static final Action save = new Action() {
        @Override
        public void does(Element element) {
            debugLog("saving...");
        }
    };

    @Override
    public Element element() {
        return new Element("page container");
    }
}
