package oopbucket.oop;

import oopbucket.core.Condition;
import oopbucket.core.Element;

import static oopbucket.core.helpers.Common.debugLog;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 10:37 PM
 */
public class RadioButtons extends Component{

    public static Condition inRadioMode(String modeValue){
        return new Condition("in radio mode " + modeValue);
    }

    public static Action select(final String modeValue){
        return new Action() {
            @Override
            public void does(Element element) {
                debugLog("selecting " + element + " to mode " + modeValue + "...");
            }
        };
    }

    @Override
    public Element element() {
        return new Element("radio buttons container");
    }
}
