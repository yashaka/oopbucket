package oopbucket.oop;

import oopbucket.core.Element;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 3:01 PM
 */
public interface Action {

    public void does(Element element);
}
