package oopbucket.oop;

import oopbucket.core.Condition;
import oopbucket.core.Element;

import static oopbucket.core.helpers.Conditions.is;
import static oopbucket.core.helpers.Conditions.shouldBe;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 2:59 PM
 */
public abstract class Component {

    public abstract Element element();

    public void ensure(Action action, Condition condition){
        if (!is(condition)){
            action.does(element());
            shouldBe(condition);
        }
    }

    public void forSure(Action action, Condition condition){
        action.does(element());
        shouldBe(condition);
    }

    public void doForSure(Action action, Element element, Condition condition){
        action.does(element());
        shouldBe(element, condition);
    }

    public void does(Action action){
        action.does(element());
    }
}
