package oopbucket.oop;

import oopbucket.core.Element;

import static oopbucket.core.helpers.Common.debugLog;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 3:12 PM
 */
public class Modal {

    public static final Action confirm = new Action() {
        @Override
        public void does(Element element) {
            debugLog("confirming modal dialog...");
        }
    };
}
