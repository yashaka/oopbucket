package oopbucket.oop;

import oopbucket.core.Element;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 3:18 PM
 */
public class ActionHelpers {

    public static Action and(final Action... actions){
        return new Action() {
            @Override
            public void does(Element element) {
                for(int i = 0; i < actions.length; i++){
                    actions[i].does(element);
                }
            }
        };
    }
}
