package oopbucket.procedural;

import oopbucket.core.Condition;
import oopbucket.core.Element;

import static oopbucket.core.helpers.Common.debugLog;
import static oopbucket.core.helpers.Conditions.shouldBe;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 10:31 PM
 */
public class RadioButtons {

    public static final Condition inRadioMode(String modeValue){
        return new Condition("in radio mode " + modeValue);
    }

    public static Element radioButtons(){
        return new Element("radioButtons");
    }

    public static void select(Element radioButtons, String modeValue){
        debugLog("selecting + " + radioButtons + " to mode: " + modeValue);
    }

    public static void selectForSure(Element radioButtons, String modeValue){
        select(radioButtons, modeValue);
        shouldBe(radioButtons, inRadioMode(modeValue));
    }
}
