package oopbucket.procedural;

import oopbucket.core.Condition;

import static oopbucket.core.helpers.Conditions.is;
import static oopbucket.core.helpers.Conditions.shouldBe;
import static oopbucket.core.helpers.Common.debugLog;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 2:38 PM
 */
public class Page {

    public static final Condition saved = new Condition("saved");

    public static void save(){
        debugLog("saving...");
    }

    public static void saveForSure(){
        save();
        shouldBe(saved);
    }

    public static void saveWithExpectedConfirmationForSure(){
        save();
        oopbucket.procedural.Modal.confirm();
        shouldBe(saved);
    }

    public static void ensureSaved(){
        if (!is(saved)){
            saveForSure();
        }
    }

    public static void ensureSavedWithConfirmation(){
        if (!is(saved)){
            saveWithExpectedConfirmationForSure();
        }
    }

}
