package oopbucket.functional;

import oopbucket.core.Condition;
import oopbucket.core.Element;

import static oopbucket.core.helpers.Common.debugLog;

/**
 * @author ykramarenko
 *         Date: 2/11/14
 *         Time: 10:14 PM
 */
public class CheckBox {

    public static Condition checked = new Condition("checked");

    public static void check(Element checkBox){
        debugLog("checking " + checkBox);
    }

    public static Element checkBox(){
        return new Element("checkbox");
    }
}
