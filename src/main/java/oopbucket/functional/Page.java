package oopbucket.functional;

import oopbucket.core.Condition;

import static oopbucket.core.helpers.Common.debugLog;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 2:38 PM
 */
public class Page {

    public static final Condition saved = new Condition("saved");

    public static void save(){
        debugLog("saving...");
    }

}
