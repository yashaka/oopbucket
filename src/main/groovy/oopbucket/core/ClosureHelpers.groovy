package oopbucket.core

/**
 * @author ykramarenko
 *         Date: 2/11/14
 *         Time: 11:07 PM
 */
class ClosureHelpers {
    public static Closure and(Closure... closures){
        return {
            for(int i = 0; i < closures.length; i++){
                closures[i].call()
            }
        }
    }

    public static Closure curried(Closure clo, Object... parameters){
        Closure res = clo
        for(int i = 0; i < parameters.length; i++){
            res = res.curry(parameters[i])
        }
        return res
    }
}
