package oopbucket.functional

import oopbucket.core.Condition
import oopbucket.core.Element

import static oopbucket.core.helpers.Conditions.is
import static oopbucket.core.helpers.Conditions.shouldBe
/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 3:53 PM
 */
class ComponentHelpers {

    public static void ensure(Condition condition, Closure action){
        if (!is(condition)){
            action.call();
            shouldBe(condition);
        }
    }

    public static void doForSure(Condition condition, Closure action){
        action.call();
        shouldBe(condition);
    }

    public static void doForSure(Element element, Condition condition, Closure action){
        action.call();
        shouldBe(element, condition)
    }
}
