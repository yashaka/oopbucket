package oopbucket.functional2

import oopbucket.core.ClosureHelpers
import oopbucket.core.Condition
import oopbucket.core.Element

import static oopbucket.core.helpers.Conditions.is
import static oopbucket.core.helpers.Conditions.shouldBe
/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 3:53 PM
 */
class ComponentHelpers {

    public static void ensure(Condition condition, Closure action){
        if (!is(condition)){
            action.call();
            shouldBe(condition);
        }
    }

    public static void forSure(Condition condition, Closure action){
        action.call();
        shouldBe(condition);
    }

    public static void forSure(Element element, Condition condition, Closure action){
        action.call();
        shouldBe(element, condition)
    }

    /**
     * passing element to action as first parameter
     * @param element
     * @param condition
     * @param action
     */
    public static void forSure(Condition condition, Closure action, Element element, Object... parameters){
        ClosureHelpers.curried(action.curry(element), parameters).call();
        shouldBe(element, condition)
    }


}
