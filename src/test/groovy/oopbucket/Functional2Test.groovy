package oopbucket
import oopbucket.functional2.CheckBox
import oopbucket.functional2.Modal
import oopbucket.functional2.Page
import oopbucket.procedural.RadioButtons

import static oopbucket.core.ClosureHelpers.and
import static oopbucket.functional2.CheckBox.checkBox
import static oopbucket.functional2.CheckBox.checked
import static oopbucket.functional2.ComponentHelpers.forSure
import static oopbucket.functional2.Page.saved
import static oopbucket.functional2.RadioButtons.radioMode
import static oopbucket.functional2.RadioButtons.radioButtons
/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 4:05 PM
 */
class Functional2Test extends GroovyTestCase {

    public void testSavePage(){
        Page.save();
    }

    public void testPageCanBeSaved(){
        forSure(saved, Page.&save())
    }

    public void testPageCanBeSavedWithConfirmation(){
        forSure(saved, and(Page.&save, Modal.&confirm))
    }

    public void testRadioButtonsCanBeSelectedToMode(){
        forSure(radioMode("enabled"), RadioButtons.&select, radioButtons(), "enabled")
    }

    public void testCheckBoxCanBeChecked(){
        forSure(checked, CheckBox.&check, checkBox())
    }
}
