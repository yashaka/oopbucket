package oopbucket
import oopbucket.functional.Modal
import oopbucket.functional.Page
import oopbucket.functional2.CheckBox

import static oopbucket.functional.ComponentHelpers.doForSure
import static oopbucket.functional.Page.saved
import static oopbucket.functional.RadioButtons.*
import static oopbucket.functional.CheckBox.checkBox
import static oopbucket.functional.CheckBox.checked
/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 4:05 PM
 */
class FunctionalTest extends GroovyTestCase {

    public void testSavePage(){
        Page.save();
    }

    public void testPageCanBeSaved(){
        doForSure(saved) { Page.save() }
    }

    public void testPageCanBeSavedWithConfirmation(){
        doForSure(saved) {
            Page.save()
            Modal.confirm()
        }
    }

    public void testRadioButtonsCanBeSelectedToMode(){
        doForSure(radioButtons(), inRadioMode("enabled")) {
            select(radioButtons(), "enabled")
        }
    }

    public void testCheckBoxCanBeChecked(){
        doForSure(checkBox(), checked, CheckBox.&check.curry(checkBox()))
    }
}
