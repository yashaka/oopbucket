package oopbucket;

import oopbucket.procedural.Page;

import static oopbucket.procedural.RadioButtons.radioButtons;
import static oopbucket.procedural.RadioButtons.selectForSure;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 2:51 PM
 */
@org.testng.annotations.Test
public class ProceduralTest {

    public void testSavePage(){
        Page.save();
    }

    public void testPageCanBeSaved(){
        Page.saveForSure();
    }

    public void testPageCanBeSavedWithConfirmation(){
        Page.saveWithExpectedConfirmationForSure();
    }

    public void testRadioButtonsCanBeSelectedToMode(){
        selectForSure(radioButtons(), "enabled");
    }
}
