package oopbucket;

import oopbucket.oop.Page;
import oopbucket.oop.RadioButtons;

import static oopbucket.oop.ActionHelpers.and;
import static oopbucket.oop.Modal.confirm;
import static oopbucket.oop.Page.saved;
import static oopbucket.oop.Page.save;
import static oopbucket.oop.RadioButtons.inRadioMode;
import static oopbucket.oop.RadioButtons.select;

/**
 * @author ykramarenko
 *         Date: 2/9/14
 *         Time: 3:23 PM
 */
@org.testng.annotations.Test
public class OOPTest {

    private final Page page = new Page();
    private final RadioButtons radioButtons = new RadioButtons();

    public void testSavePage(){
        page.does(save);
    }

    public void testPageCanBeSaved(){
        page.forSure(save, saved);
    }

    public void testPageCanBeSavedWithConfirmation(){
        page.forSure(and(save, confirm), saved);
    }

    public void testRadioButtonsCanBeSelectedToMode(){
        radioButtons.forSure(select("enabled"), inRadioMode("enabled"));
    }
}
